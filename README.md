# pymd's 'Joe's window manager configs'


>I'm still only experementing at the moment with Jwm, it's very much a work in progress, I will continue to update it here. Though, I wouldn't think it quite ready for anyone to use as their main config. 

**to install Jwm** (The best window manager in my opinion.)

**Debian/ubuntu**
`sudo apt-get install jwm`

**Arch**
`sudo pacman -S jwm`

obviously if you were to compile 'jwm' yourself or get it from the *aur* it would be a much more up to date version.


I've added to jwm, to make it a little more useable: 

* **dmenu** *one can now use the key bindings, Alt+D which will spawn the menu*
* **Rofi** *I added rofi, use the key bindings alt+r, this will spawm rofi*
* **Keybindings** added. 
* **xdgmenmaker** added. *one can now add menu items, and they will automatically be added to the menu and 
will have an Icon added*
* **plank** *plank has been added to autostart.* 
* *follow* this link to install xdgmenmaker the easy way: <https://notabug.org/adnan360/jwm-config> 
* **nitrogen** added as the default wallpaper chooser. (One can choose whether to enable *nitrogen* or *Variety* within the jwm.rc, just comment out the one you don't wish to use).
* **Maato Volume-Icon** a great system tray app, I really do recommend this app!

**Links to install ** Maato Volume icon**
`git clone https://github.com/Maato/volumeicon.git`

` $ ./autogen.sh`
 ` $ ./configure --prefix=/usr`
 ` $ make`
 ` $ sudo make install` 

 **link to repo:** <https://github.com/Maato/volumeicon> *please visit this page for more info*





**to install plank:**

**Debian/Ubuntu**
`sudo apt-get install plank`

**Arch**
`Sudo pacman -S plank`

**to install nitrogen**
**Debian/ubuntu**
`sudo apt-get install nitrogen`

**Arch**
`sudo pacman -S nitrogen`

**to install variety**

**Debian/Ubuntu**
`sudo apt-get install variety`
**Arch**
`sudo pacman -S variety`

**Themes added** (In the pymdsthemes folder)
* Cyan blue theme
* jwm.rc *a theme in light solarized colours*

**to-do**
~~>make a wifi taskbar shortcut~~

**note:** *if you like to use the wonderful arcolinux configs along with my changes, please follow the readme instructions that are in the **my arcolinux jwm configs** folder*

---


