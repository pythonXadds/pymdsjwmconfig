In order to use these Jwm config files you need to place each of them into the correct folder and location.

**Jwm** needs to be placed in the `.config` folder in your home folder.
**.jwmrc** must be copied to your ** *home* ** directory.

These things must be done in order for them to work correctly.